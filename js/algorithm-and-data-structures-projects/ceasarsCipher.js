function rot13(str) {
  return String.fromCodePoint(
    ...str
      .split("")
      .map((ch) => ch.charCodeAt(0))
      .map((c) => (c < 65 ? c : c <= 77 ? c + 13 : 65 + (12 - (90 - c))))
  );
}

module.exports = rot13;
