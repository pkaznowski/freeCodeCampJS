function convertToRoman(num) {
  let result = "";
  const arabToRom = [
    [1000, "M"], [900, "CM"], [500, "D"], [400, "CD"], [100, "C"], [90, "XC"],
    [50, "L"], [40, "XL"], [10, "X"], [9, "IX"], [5, "V"], [4, "IV"], [1, "I"],
  ];

  for (let k in arabToRom) {
    let [arab, rom] = arabToRom[k];
    if (num >= arab) {
      result += Array(Math.floor(num / arab) + 1).join(rom);
      num = num % arab;
    }
  }
  return result;
}


module.exports = convertToRoman;
