/*
Return {status: "INSUFFICIENT_FUNDS", change: []} if cash-in-drawer is less than
the change due, or if you cannot return the exact change.

Return {status: "CLOSED", change: [...]} with cash-in-drawer as the value for
the key change if it is equal to the change due.

Otherwise, return {status: "OPEN", change: [...]}, with the change due in coins
and bills, sorted in highest to lowest order, as the value of the change key.
*/

function checkCashRegister(price, cash, cid) {
  // js makes CRAZY stuff with numbers o_O, let's fix that nonsense
  const round = (n) => Math.round((n + Number.EPSILON) * 100) / 100;
  const currencies = {
    "ONE HUNDRED": 100, TWENTY: 20, TEN: 10, FIVE: 5, ONE: 1, QUARTER: 0.25,
    DIME: 0.1, NICKEL: 0.05, PENNY: 0.01,
  };

  let due = cash - price;
  const total = round(cid.map((e) => e[1]).reduce((a, b) => a + b));

  let result = { status: "INSUFFICIENT_FUNDS", change: [] };

  if (due > total) {
    return result;
  }

  result.status = due == total ? "CLOSED" : "OPEN";

  let change = [];

  for (const unit in currencies) {
    let in_drawer = cid.filter((x) => x[0] == unit);

    // if there's no bills/coins of that currency in the drawer, skip
    if (in_drawer.length == 0) { continue; }

    // it's nested, grab the value
    in_drawer = in_drawer[0][1];

    let amount_given = 0;

    // if we can, we give the biggest unit available
    if (currencies[unit] <= due && in_drawer) {

      for (let i = 0; i < in_drawer / currencies[unit]; i++) {
        amount_given += currencies[unit];
        due -= currencies[unit];
        if (round(due - currencies[unit]) < 0) { break; }
      }

      change.push([unit, round(amount_given)]);

    }

    if (round(due) == 0) {
      result.change = result.status == "CLOSED" ? cid : change;
      return result;
    }
  }

  result.status = "INSUFFICIENT_FUNDS";
  return result;
}

module.exports = checkCashRegister;
