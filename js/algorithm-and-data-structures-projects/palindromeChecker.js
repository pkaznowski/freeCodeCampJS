function palindrome(str) {
  str = str.replace(/\W+|_+/g, "").toLowerCase().split("");
  for (let i = 0; i < str.length / 2; i++) {
    if (str[i] != str[str.length - i - 1]) {
      return false;
    }
  }
  return true;
}

// const idx = Math.floor(str.length / 2);
// const [a, b] = [
//   str.slice(0, idx),
//   str.slice(str.length % 2 == 0 ? idx : idx + 1),
// ];
// b.reverse();
// return a.join() == b.join();

module.exports = palindrome;
