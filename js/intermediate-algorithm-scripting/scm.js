/* NOTES:
  - generators and how to parse them:
    https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Iterators_and_Generators
*/

function* lcm(x, y) {
  let greater = y;
  while (true) {
    if (greater % x == 0 && greater % y == 0) {
      yield greater;
    }
    greater++;
  }
}

function smallestCommons(arr) {
  const [x, y] = arr.sort((a, b) => a - b);

  for (let m of lcm(x, y)) {
    let pass = true;

    for (let i = x + 1; i < y; i++) {
      if (m % i != 0) {
        pass = false;
      }
    }

    if (pass) {
      return m;
    }
  }
}

module.exports = smallestCommons;

