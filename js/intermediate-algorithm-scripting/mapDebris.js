/* NOTES:
   - orbital periods: https://en.wikipedia.org/wiki/Orbital_period
   - formula: 
     T = 2π√(a³/GM)
     where: a -- orbit's semi major axis
     for some reason freeCodeCamp interprets `a` as earthRadius + avgAlt
*/

function orbitalPeriod(arr) {
  const GM = 398600.4418;
  const earthRadius = 6367.4447;
  const period = (axis) =>
    Math.round(Math.PI * 2 * Math.sqrt(Math.pow(axis, 3) / GM));

  let result = [];
  for (const o in arr) {
    let axis = arr[o].avgAlt + earthRadius;
    result.push({ name: arr[o].name, orbitalPeriod: period(axis) });
  }
  return result;
}

module.exports = orbitalPeriod;
