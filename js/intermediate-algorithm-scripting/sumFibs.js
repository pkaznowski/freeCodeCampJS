function fibs(lessThan) {
  const fib = (x) => {
    return x === 0 ? 0 : x === 1 ? 1 : fib(x - 1) + fib(x - 2);
  };

  let arr = [];
  let x = 1;

  while (fib(x) <= lessThan) {
    arr.push(fib(x));
    x++;
  }

  return arr;
}

function sumFibs(num) {
  return fibs(num)
    .filter((n) => n % 2 != 0)
    .reduce((a, b) => a + b);
}

module.exports = { fibs: fibs, sum: sumFibs };
