function steamrollArray(arr) {
  let narr = [...arr];

  while (narr.some((e) => e instanceof Array)) {
    arr = narr;
    for (const e in arr) {
      if (arr[e] instanceof Array) {
        narr.splice(e, 1, ...arr[e]);
      }
    }
  }
  return narr;
}

module.exports = steamrollArray;
