function addTogether(x, y) {
  const isN = (a) => typeof a == "number";
  const add = (n) => (isN(n) ? x + n : undefined);
  return isN(x)
    ? Object.keys(arguments).length < 2
      ? add
      : add(y)
    : undefined;
}

module.exports = addTogether;
