function convertHTML(str) {
  const SYM = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': "&quot;",
    "'": "&apos;",
  };
  for (const e in SYM) {
    const re = new RegExp(e, "g");
    str = str.replace(re, SYM[e]);
  }
  return str;
}

module.exports = convertHTML;
