function steamrollArray(arr) {
  for (const e in arr) {
    if (arr[e] instanceof Array) {
      arr = arr.splice(e, 1, ...arr[e]);
      console.log(arr);
      return steamrollArray(arr);
    }
  }
  return arr;
}

console.log(steamrollArray([1, [2], [3, [[4]]]]));

module.exports = steamrollArray;
