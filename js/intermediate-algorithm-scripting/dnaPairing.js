function pairElement(str) {
  const Pair = {
    A: "T",
    T: "A",
    C: "G",
    G: "C",
  };
  let paired = [];
  for (const c in str) {
    paired.push([str[c], Pair[str[c]]]);
  }
  return paired;
}

module.exports = pairElement;
