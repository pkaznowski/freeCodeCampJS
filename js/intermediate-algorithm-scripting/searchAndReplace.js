/* NOTES:
  - negative lookBehind: (?:<class>)<match>
  - str.replace(str|regex, str|func returning str)
*/

function myReplace(str, before, after) {
  if (before[0] === before[0].toUpperCase()) {
    after = after.replace(/(?:^)\S/, (c) => c.toUpperCase());
  }
  return str.replace(before, after);
}

module.exports = myReplace;

