function truthCheck(collection, pre) {
  for (e in collection) {
    let x = collection[e];
    if (!(x.hasOwnProperty(pre) && x[pre])) {
      return false;
    }
  }
  return true;
}

module.exports = truthCheck;
