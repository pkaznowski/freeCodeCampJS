function binaryAgent(str) {
  return str
    .split(" ")
    .map((w) => parseInt(w, 2))
    .map((n) => String.fromCharCode(n))
    .join("");
}

module.exports = binaryAgent;
