function uniteUnique(...arr) {
  let all = arr.flat();
  let union = [];

  for (e in all) {
    if (union.indexOf(all[e]) < 0) {
      union.push(all[e]);
    }
  }

  return union;
}

module.exports = uniteUnique;
