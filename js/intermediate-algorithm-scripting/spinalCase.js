function spinalCase(str) {
  return str
    .replace(/([A-Z])/g, ` $1`)
    .replace(/_/g, " ")
    .trim()
    .split(/\W+/)
    .join("-")
    .toLowerCase();
}

module.exports = spinalCase;
