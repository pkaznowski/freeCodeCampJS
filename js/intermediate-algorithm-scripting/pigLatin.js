function translatePigLatin(str) {
  const RE = /[aeiou]/i;
  if (RE.test(str)) {
    const idx = str.match(RE).index;
    let w = idx === 0 ? "w" : "";
    return str.slice(idx) + str.slice(0, idx) + `${w}ay`;
  }
  return str + "ay";
}

module.exports = translatePigLatin;

