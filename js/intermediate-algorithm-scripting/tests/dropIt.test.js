const drop = require("../dropIt");

test("matches fCC data", () => {
  expected = [
    [[1, 2, 3, 4],    function (n) { return n >= 3; },  [3, 4]],
    [[0, 1, 0, 1],    function (n) { return n === 1; }, [1, 0, 1]],
    [[1, 2, 3],       function (n) { return n > 0; },   [1, 2, 3]],
    [[1, 2, 3, 4],    function (n) { return n > 5; },   []],
    [[1, 2, 3, 7, 4], function (n) { return n > 3; },   [7, 4]],
    [[1, 2, 3, 9, 2], function (n) { return n > 2; },   [3, 9, 2]],
  ];

  for (const k in expected) {
    const [arr, fun, result] = expected[k];
    expect(drop(arr, fun)).toEqual(result);
  }
});
