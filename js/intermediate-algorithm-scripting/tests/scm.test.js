const smallestCommons = require("../scm");

test("returns correct lcm for given data", () => {
  const expected = {
    "[1,5]": 60,
    "[5,1]": 60,
    "[2,10]": 2520,
    "[10,2]": 2520,
    "[13,1]": 360360,
    "[18,23]": 6056820,
  };
  for (k in expected) {
    expect(smallestCommons(eval(k))).toBe(expected[k]);
  }
});
