const myReplace = require("../searchAndReplace");

test("replaces string without changing the case", () => {
  expect(
    myReplace("A quick brown fox jumped over", "jumped", "leaped")
  ).toBe("A quick brown fox leaped over");
});

test("replaces string with changing the case", () => {
  expect(
    myReplace("A quick brown fox Jumped over", "Jumped", "leaped")
  ).toBe("A quick brown fox Leaped over");
});
