const pairElement = require("../dnaPairing");

test("pairs all basic elements", () => {
  expect(pairElement("AGTCGATC")).toEqual([
    ["A", "T"],
    ["G", "C"],
    ["T", "A"],
    ["C", "G"],
    ["G", "C"],
    ["A", "T"],
    ["T", "A"],
    ["C", "G"],
  ]);
});
