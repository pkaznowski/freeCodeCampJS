const flat = require("../steamroller");

test("matches fCC data", () => {
  expected = [
    [ [[["a"]], [["b"]]],   ["a", "b"]    ],
    [ [1, [2], [3, [[4]]]], [1, 2, 3, 4]  ],
    [ [1, [], [3, [[4]]]],  [1, 3, 4]     ],
    [ [1, {}, [3, [[4]]]],  [1, {}, 3, 4] ],
  ];

  for (const k in expected) {
    const [arr, result] = expected[k];
    expect(flat(arr)).toEqual(result);
  }
});
