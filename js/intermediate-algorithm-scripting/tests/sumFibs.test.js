const sumFibs = require("../sumFibs");

test("returns correct array of Fibonacci numbers", () => {
  expect(sumFibs.fibs(10)).toEqual([1, 1, 2, 3, 5, 8]);
});

test("returns sum of Fibonacci numbers in given range", () => {
  const expected = {
    1: 2,
    4: 5,
    1000: 1785,
    75024: 60696,
    75025: 135721,
    4000000: 4613732,
  };

  for (const k in expected) {
    expect(sumFibs.sum(k)).toEqual(expected[k]);
  }
});
