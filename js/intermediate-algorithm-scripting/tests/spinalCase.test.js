const spinalCase = require("../spinalCase");

test("deals with camelcase strings", () => {
  expect(spinalCase("CamelCaseString")).toBe("camel-case-string");
});

test("deals with non letter chars", () => {
  expect(
    spinalCase("this.is.a_string:with non? letters")
  ).toBe("this-is-a-string-with-non-letters");
});
