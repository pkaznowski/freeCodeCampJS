const pigLatin = require("../pigLatin");

test("piggifies word starting with a vowel", () => {
  expect(pigLatin("ale")).toBe("aleway");
});

test("piggifies word starting with consonants", () => {
  expect(pigLatin("croissant")).toBe("oissantcray");
});

test("piggifies word without vowels", () => {
  expect(pigLatin("rythm")).toBe("rythmay");
});
