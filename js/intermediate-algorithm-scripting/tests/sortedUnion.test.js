const uniteUnique = require("../sortedUnion");

test("test passes with filtering", () => {
  expect(
    uniteUnique([1, 2, 3], [5, 2, 1, 4], [2, 1], [6, 7, 8])
  ).toEqual([1, 2, 3, 5, 4, 6, 7, 8]);
});

test("test passes without filtering", () => {
  expect(uniteUnique([1, 2, 3], [4, 5, 6])).toEqual([1, 2, 3, 4, 5, 6]);
});
