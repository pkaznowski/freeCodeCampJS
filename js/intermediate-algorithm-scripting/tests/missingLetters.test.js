const fearNotLetter = require("../missingLetters");

test("returns missing letter", () => {
  expect(fearNotLetter("abce")).toBe("d");
});

test("returns undefined when letters not missing", () => {
  expect(fearNotLetter("abcde")).toBe(undefined);
});
