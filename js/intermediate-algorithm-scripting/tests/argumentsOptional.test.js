const addTogether = require("../argumentsOptional");

test("matches fCC data", () => {
  expect(addTogether(2, 3)).toBe(5);
  expect(addTogether(23, 30)).toBe(53);
  expect(addTogether(5)(7)).toBe(12);
  expect(addTogether("http://bit.ly/IqT6zt")).toBe(undefined);
  expect(addTogether(2, "3")).toBe(undefined);
  expect(addTogether(2)([3])).toBe(undefined);
});
