const Person = require("../makePerson");

test("matches fCC data", () => {
  bob = new Person("Bob Ross");

  expect(Object.keys(bob).length).toBe(6);
  expect(bob instanceof Person).toBe(true);
  expect(bob.firstName).toBe(undefined);
  expect(bob.lastName).toBe(undefined);
  expect(bob.getFirstName()).toBe("Bob");
  expect(bob.getLastName()).toBe("Ross");
  expect(bob.getFullName()).toBe("Bob Ross");

  bob.setFirstName("Haskell");
  expect(bob.getFullName()).toBe("Haskell Ross");

  bob.setLastName("Curry");
  expect(bob.getFullName()).toBe("Haskell Curry");

  bob.setFullName("Haskell Curry");
  expect(bob.getFullName()).toBe("Haskell Curry");

  bob.setFullName("Haskell Curry");
  expect(bob.getFirstName()).toBe("Haskell");

  bob.setFullName("Haskell Curry");
  expect(bob.getLastName()).toBe("Curry");
});
