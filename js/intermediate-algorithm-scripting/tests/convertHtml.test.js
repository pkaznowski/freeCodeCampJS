const convertHTML = require("../convertHtml");

test("converts multiple chars", () => {
  expect(convertHTML("a > b > c")).toBe("a &gt; b &gt; c");
});

test("converts different chars", () => {
  expect(convertHTML("<>'&\"")).toBe("&lt;&gt;&apos;&amp;&quot;");
});
