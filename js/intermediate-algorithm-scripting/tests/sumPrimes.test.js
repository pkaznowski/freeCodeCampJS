const sumPrimes = require("../sumPrimes");

test("returns correct boolean for some primes", () => {
  numbersToPrimes = {
    2: true,
    3: true,
    4: false,
    5: true,
    7: true,
    8: false,
    9: false,
    11: true,
  };
  for (k in numbersToPrimes) {
    expect(sumPrimes.is(k)).toBe(numbersToPrimes[k]);
  }
});

test("returns list of primes til a number", () => {
  expect(sumPrimes.get(20)).toEqual([2, 3, 5, 7, 11, 13, 17, 19]);
});

test("returns correct sum of primes til given number", () => {
  expected = {
    10: 17,
    977: 73156,
  };
  for (k in expected) {
    expect(sumPrimes.sum(k)).toBe(expected[k]);
  }
});
