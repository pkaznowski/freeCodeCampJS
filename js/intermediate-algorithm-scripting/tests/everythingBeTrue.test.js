const truthCheck = require("../everythingBeTrue");

test("matches fCC data", () => {
  const expected = [
    [
      [
        { user: "Tinky-Winky", sex: "male" },
        { user: "Dipsy", sex: "male" },
        { user: "Laa-Laa", sex: "female" },
        { user: "Po", sex: "female" },
      ],
      "sex",
      true,
    ],
    [
      [
        { user: "Tinky-Winky", sex: "male" },
        { user: "Dipsy" },
        { user: "Laa-Laa", sex: "female" },
        { user: "Po", sex: "female" },
      ],
      "sex",
      false,
    ],
    [
      [
        { user: "Tinky-Winky", sex: "male", age: 0 },
        { user: "Dipsy", sex: "male", age: 3 },
        { user: "Laa-Laa", sex: "female", age: 5 },
        { user: "Po", sex: "female", age: 4 },
      ],
      "age",
      false,
    ],
    [
      [
        { name: "Pete", onBoat: true },
        { name: "Repeat", onBoat: true },
        { name: "FastForward", onBoat: null },
      ],
      "onBoat",
      false,
    ],
    [
      [
        { name: "Pete", onBoat: true },
        { name: "Repeat", onBoat: true, alias: "Repete" },
        { name: "FastForward", onBoat: true },
      ],
      "onBoat",
      true,
    ],
    [[{ single: "yes" }], "single", true],
    [[{ single: "" }, { single: "double" }], "single", false],
    [[{ single: "double" }, { single: undefined }], "single", false],
    [[{ single: "double" }, { single: NaN }], "single", false],
  ];

  for (e in expected) {
    const [coll, pre, result] = expected[e];
    expect(truthCheck(coll, pre)).toBe(result);
  }
});
