function fearNotLetter(str) {
  let last = str.charCodeAt(0);
  for (let c = 1; c < str.length; c++) {
    let cur = str.charCodeAt(c);
    // typeof(c) -> string o_O
    if (cur - last > 1) {
      return String.fromCharCode(last + 1);
    }
    last = cur;
  }
}

module.exports = fearNotLetter;
