function isPrime(num) {
  for (let i = 2; i < num; i++) {
    if (num % i == 0) {
      return false;
    }
  }
  return true;
}

function getPrimesTil(num) {
  let primes = [];
  for (let i = 2; i <= num; i++) {
    if (isPrime(i)) {
      primes.push(i);
    }
  }
  return primes;
}

function sumPrimes(num) {
  return getPrimesTil(num).reduce((a, b) => a + b);
}

module.exports = {
  is: isPrime,
  get: getPrimesTil,
  sum: sumPrimes,
};
