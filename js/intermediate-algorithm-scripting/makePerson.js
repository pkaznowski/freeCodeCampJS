var Person = function (firstAndLast) {
  // Only change code below this line
  // Complete the method below and implement the others similarly
  let first = firstAndLast.split(" ")[0];
  let last = firstAndLast.split(" ")[1];

  this.getFullName = () => `${first} ${last}`;
  this.getFirstName = () => first;
  this.getLastName = () => last;
  this.setFirstName = (f) => (first = f);
  this.setLastName = (l) => (last = l);
  this.setFullName = (firstAndLast) =>
    ([first, last] = firstAndLast.split(" "));
};

module.exports = Person;
